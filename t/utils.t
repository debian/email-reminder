#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 15;
use EmailReminder::Utils;

# just do a selection
my %year = (
    1 => { th => 'st', special => '(Paper) ' },
    2 => { th => 'nd', special => '(Cotton) ' },
    3 => { th => 'rd', special => '(Leather) ' },
    4 => { th => 'th', special => '(Linen) ' },
    5 => { th => 'th', special => '(Wood) ' },
    10 => { th => 'th', special => '(Tin) ' },
    60 => { th => 'th', special => '(Diamond) ' },
);

# just do a selection of dates
foreach my $year ( sort { $a <=> $b } keys %year ) {
    is(EmailReminder::Utils::get_th($year), $year{$year}->{th}, "$year index");
    is(EmailReminder::Utils::get_special_name($year), $year{$year}->{special}, "$year special");
}

is(EmailReminder::Utils::get_special_name(22), undef, "22 index");
