#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 12;
use EmailReminder::EventList;

my $salutation = 'Hi there,

';
my $footer = 'Have a good day!

--
Sent by Email-Reminder '.$EmailReminder::Utils::VERSION.'
https://launchpad.net/email-reminder
';

my @strs = (
'I just want to remind you of the following event :

5-a-side

',
'I just want to remind you of the following event :

5-a-side

',
'I just want to remind you of the following event :

Christmas Day

',
'I just want to remind you of the following event :

17th New Year\'s Day

',
'I just want to remind you that the 23rd anniversary of My Sister and Bro in Law is .

You can reach My Sister at sis.bro-in-law@exmaple.org.

',
'I just want to remind you that the 27th anniversary of My Brother and Sis in Law is .

You can reach them at bro.sis-in-law@example.com and sis-in-law@example.com respectively.

',
'I just want to remind you of the following event :

PayDay

',
'I just want to remind you of the following event :

Clean the House

',
'I just want to remind you that Sister is turning 44 .

You can reach Sister at sister@example.org.

',
'I just want to remind you that Bro is turning 47 .

You can reach Bro at bro@example.org.

',
'I just want to remind you that Friend is turning 48 .

You can reach Friend at friend@example.com.

',
);

for (my $i=0; $i < scalar(@strs); $i++) {
    $strs[$i] = $salutation . $strs[$i] . $footer;
}

# tests
my $events = EmailReminder::EventList->new('t/data.xml', 1);

# loop through all the events themselves
foreach my $event ($events->get_events()) {
    my $msg = $event->get_message;
    my ($i) = grep { $strs[$_] eq $msg } 0..$#strs;
    splice (@strs, $i, 1) if defined($i);
    ok(defined($i), "event msg ($i)");
}
is(scalar @strs, 0, "all messages were found");
