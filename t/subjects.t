#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 12;
use EmailReminder::EventList;

my @strs = (
    '5-a-side',
    '5-a-side',
    'Christmas Day',
    'New Year\'s Day',
    '23rd anniversary of My Sister and Bro in Law',
    '27th anniversary of My Brother and Sis in Law',
    'PayDay',
    'Clean the House',
    'Sister\'s birthday',
    'Bro\'s birthday',
    'Friend\'s birthday',
);

# tests
my $events = EmailReminder::EventList->new('t/data.xml', 1);

# loop through all the events themselves
foreach my $event ($events->get_events()) {
    my ($i) = grep { $strs[$_] eq $event->get_subject } 0..$#strs;
    splice (@strs, $i, 1);
    ok(defined($i), "event $i");
}
is(scalar @strs, 0, "all subjects were found");
