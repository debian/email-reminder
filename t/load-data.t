#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Test::More tests => 19;
use EmailReminder::EventList;
use EmailReminder::Event;

my @strs = (
    'weekly:7) 5-a-side - 2',
    'weekly:8) 5-a-side - 6',
    'yearly:5) Christmas Day - 12-25',
    'yearly:6) New Year\'s Day - 2008-01-01',
    'anniversary:3) My Sister and Bro in Law - 2000-10-20',
    'anniversary:4) My Brother and Sis in Law - 1997-02-01',
    'monthly:0) PayDay - 16',
    'monthly:1) Clean the House - 1',
    'birthday:2) Sister - 1980-07-04',
    'birthday:9) Bro - 1976-11-23',
    'birthday:10) Friend - 1976-02-29',
);

# tests
my $events = EmailReminder::EventList->new('t/data.xml', 1);

is(join(' ', $events->get_user_name), 'My Name', 'user name');
is($events->get_user_email, 'my.name@example.org', 'user email');

# check the stores
my $anniversary_model = $events->get_model("anniversary");
is($anniversary_model->get_nb_events(), 2, 'anniversary events');

my $birthday_model = $events->get_model("birthday");
is($birthday_model->get_nb_events(), 3, 'birthday events');

my $monthly_model = $events->get_model("monthly");
is($monthly_model->get_nb_events(), 2, 'monthly events');

my $weekly_model = $events->get_model("weekly");
is($weekly_model->get_nb_events(), 2, 'weekly events');

my $yearly_model = $events->get_model("yearly");
is($yearly_model->get_nb_events(), 2, 'yearly events');

# loop through all the events themselves
foreach my $event ($events->get_events()) {
    my ($i) = grep { $strs[$_] eq "$event" } 0..$#strs;
    splice (@strs, $i, 1);
    ok(defined($i), "event rendering ($i)");
}
is(scalar @strs, 0, "all events were found");
