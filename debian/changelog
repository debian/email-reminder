email-reminder (0.8.3-1) unstable; urgency=medium

  * New upstream release
  * Install upstream AppStream metadata.
  * Bump copyright year in debian/copyright.
  * Add generated manpages to debian/clean (closes: #1044245).

 -- Francois Marier <francois@debian.org>  Fri, 08 Sep 2023 23:41:28 -0700

email-reminder (0.8.2-2) unstable; urgency=medium

  * Bump Standards-Version up to 4.6.2.
  * Bump copyright year in debian/copyright.

 -- Francois Marier <francois@debian.org>  Thu, 26 Jan 2023 20:49:25 -0800

email-reminder (0.8.2-1) unstable; urgency=medium

  * New upstream release (drop all patches)
  * Bump Standards-Version up to 4.6.1.
  * Bump copyright years in debian/copyright.

 -- Francois Marier <francois@debian.org>  Mon, 05 Dec 2022 00:23:09 -0800

email-reminder (0.8.1-4) unstable; urgency=medium

  * Fix preferences when EMAIL envvar is unset (LP: #1983060).

 -- Francois Marier <francois@debian.org>  Thu, 28 Jul 2022 21:32:59 -0700

email-reminder (0.8.1-3) unstable; urgency=medium

  * Add missing .desktop file (closes: #986744).

 -- Francois Marier <francois@debian.org>  Sat, 10 Apr 2021 19:26:37 -0700

email-reminder (0.8.1-2) unstable; urgency=medium

  * Bump Standards-Version up to 4.5.1.
  * Bump debian/watch up to version 4.
  * Add upstream metadata file.

 -- Francois Marier <francois@debian.org>  Mon, 18 Jan 2021 22:01:41 -0800

email-reminder (0.8.1-1) unstable; urgency=high

  * New upstream release:
    - fix for broken handling of some user email addresses in GUI
  * Add logcheck rule to hide normal su messages due to cronjob.

 -- Francois Marier <francois@debian.org>  Sat, 19 Sep 2020 11:32:50 -0700

email-reminder (0.8.0-1) unstable; urgency=medium

  * New upstream version
    - drop debian/patches/remove-editor.patch (applied upstream)
  * Add Python dependencies to debian/control.
  * Build with Python 3 in debian/rules.
  * Convert debian/install to debian/manpages.

 -- Francois Marier <francois@debian.org>  Tue, 08 Sep 2020 23:18:36 -0700

email-reminder (0.7.8-8) unstable; urgency=medium

  * Bump Standards-Version to 4.5.0.
  * Bump debhelper-compat to 13.
  * Run wrap-and-sort.
  * Fix case of Vcs-Git field in debian/control.

 -- Francois Marier <francois@debian.org>  Fri, 21 Aug 2020 22:04:12 -0700

email-reminder (0.7.8-7) unstable; urgency=medium

  * Patch send-reminders to work without UI.

 -- Francois Marier <francois@debian.org>  Sun, 13 Oct 2019 10:20:07 -0700

email-reminder (0.7.8-6) unstable; urgency=medium

  * Remove GTK+ editor (closes: #912879).
  * Bump Standards-Version to 4.4.1.
  * Set "Rules-Requires-Root: no" in debian/control.
  * Move collect-reminders manpage to section 8 (lintian warning).

 -- Francois Marier <francois@debian.org>  Sat, 12 Oct 2019 19:54:23 -0700

email-reminder (0.7.8-5) unstable; urgency=medium

  * Bump copyright year in debian/copyright
  * Bump Standards-Version to 4.3.0
  * Bump debhelper compat to 12
  * Trim trailing whitespace.
  * Re-export upstream signing key without extra signatures.
  * Bump copyright year in debian/copyright

 -- Francois Marier <francois@debian.org>  Fri, 01 Mar 2019 21:55:36 -0800

email-reminder (0.7.8-4) unstable; urgency=medium

  * Update VCS URLs for salsa
  * Replace exim4 with default-mta in dependencies
  * Move debian/copyright URL to HTTPS (lintian warning)
  * Remove trailing whitespace in debian/changelog (lintian warning)
  * Bump debhelper compat to 11
  * Bump Standards-Version to 4.1.4

 -- Francois Marier <francois@debian.org>  Sun, 06 May 2018 14:17:03 -0700

email-reminder (0.7.8-3) unstable; urgency=medium

  * Run wrap-and-sort
  * Add Brazillian translation (closes: #829338)
  * Move VCS URLs to HTTPS
  * Remove menu file in favor of desktop file
  * Bump Standards-Version up to 3.9.8

 -- Francois Marier <francois@debian.org>  Sun, 03 Jul 2016 15:03:36 -0700

email-reminder (0.7.8-2) unstable; urgency=medium

  * Add Dutch debconf translation (closes: #767237)

 -- Francois Marier <francois@debian.org>  Thu, 30 Oct 2014 10:01:40 +1300

email-reminder (0.7.8-1) unstable; urgency=medium

  * New upstream release (closes: #629631, #746617)

 -- Francois Marier <francois@debian.org>  Thu, 09 Oct 2014 23:46:59 +1300

email-reminder (0.7.7-4) unstable; urgency=medium

  * Update homepage in debian/control
  * Bump Standards-Version to 3.9.6

 -- Francois Marier <francois@debian.org>  Thu, 18 Sep 2014 22:50:12 +1200

email-reminder (0.7.7-2) unstable; urgency=medium

  * Force a shell for the daily cronjob's send-reminders su call

 -- Francois Marier <francois@debian.org>  Tue, 28 Jan 2014 10:42:36 +1300

email-reminder (0.7.7-1) unstable; urgency=medium

  * New upstream release
  * Add Japanese translation (closes: #692207)

  * debian/postinst: give the system user a shell of /bin/false
  * debian/postrm: avoid removing the system user when upgrading

  * Bump Standards-Version up to 3.9.5
  * Update debian/copyright and make it machine-readable
  * Use canonical VCS URLs in debian/control
  * Update watch file and add upstream's GPG signing key

 -- Francois Marier <francois@debian.org>  Mon, 27 Jan 2014 02:46:25 +1300

email-reminder (0.7.6-5) unstable; urgency=low

  * Switch to a minimal debian/rules file
  * Bump debhelper compatibility to 9
  * Bump Standards-Version up to 3.9.3

 -- Francois Marier <francois@debian.org>  Mon, 28 May 2012 22:56:32 +1200

email-reminder (0.7.6-4) unstable; urgency=low

  * Remove unnecessary versioned perl dependency
  * Add Danish debconf translation (closes: #658298)
  * Bump Standars-Version up to 3.9.2
  * Add empty build-arch and build-indep targets in debian/rules

 -- Francois Marier <francois@debian.org>  Sat, 04 Feb 2012 19:47:34 +1300

email-reminder (0.7.6-3) unstable; urgency=low

  * Bump Standars-Version up to 3.9.1
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Wed, 22 Dec 2010 15:56:45 +1300

email-reminder (0.7.6-2) unstable; urgency=low

  * Fix watch file due to change on the Google Code site (see #581622)
  * Replace makemaker PREFIX with DESTDIR (lintian warning)
  * Switch to 3.0 (quilt) source package format

 -- Francois Marier <francois@debian.org>  Fri, 21 May 2010 12:43:32 +1200

email-reminder (0.7.6-1) unstable; urgency=low

  * New upstream release
    - fix for yearless events

  * Bump Standards-Version to 3.8.4
  * Explicitly set -e in debian/config

 -- Francois Marier <francois@debian.org>  Sun, 31 Jan 2010 19:25:49 +1300

email-reminder (0.7.5-3) unstable; urgency=low

  * Update Swedish debconf translation (closes: #522983)
  * Bump Standards-Version to 3.8.2
  * Bump debhelper compatibility to 7
  * debian/rules: use dh_prep and remove deprecated dh_desktop
  * debian/post*: use "true" instead of "/bin/true" (lintian warning)

 -- Francois Marier <francois@debian.org>  Sun, 21 Jun 2009 22:44:31 +1200

email-reminder (0.7.5-2) unstable; urgency=high

  * Fix config parsing to match the output of debconf. This fixes a
    problem introduced in 0.7.5-1 where SSL would be turned on if the
    "smtp_ssl" config variable was set to "false".
  * Urgency high because it breaks most emails sent out.

 -- Francois Marier <francois@debian.org>  Sun, 15 Mar 2009 11:46:29 +1300

email-reminder (0.7.5-1) unstable; urgency=medium

  * New Upstream Version
    - fix broken Anniversary events (hence the urgency)

  * Update upstream email address and copyright year
  * Add misc:Depends to the dependencies (lintian warning)
  * Add a debconf question for SMTP SSL support (closes: #516556)

  * New Indonesian debconf translation
  * Updated Basque debconf translation (closes: #518290)
  * Updated Czech debconf translation (closes: #517539)
  * Updated Finnish debconf translation (closes: #518191)
  * Updated French debconf translation (closes: #518316)
  * Updated Galician debconf translation (closes: #517511)
  * Updated German debconf translation (closes: #517275)
  * Updated Italian debconf translation (closes: #517649)
  * Updated Portugese debconf translation (closes: #517531)
  * Updated Russian debconf translation (closes: #518618)
  * Updated Spanish debconf translation (closes: #518638)
  * Updated Vietnamese debconf translation (closes: #517643)

 -- Francois Marier <francois@debian.org>  Wed, 11 Mar 2009 14:10:12 +1300

email-reminder (0.7.4-2) unstable; urgency=low

  * Add libnet-smtp-ssl-perl to Recommends (closes: #507348)

 -- Francois Marier <francois@debian.org>  Mon, 01 Dec 2008 09:10:16 +1300

email-reminder (0.7.4-1) unstable; urgency=low

  * New Upstream Version
  * Don't try to install upstream TODO since it's not there anymore

 -- Francois Marier <francois@debian.org>  Fri, 21 Nov 2008 09:12:12 +1300

email-reminder (0.7.3-4) unstable; urgency=low

  * Switch packaging repo to git
  * Bump Standards-Version to 3.8.0 (no changes)
  * New Spanish debconf translation, thanks to Fran (closes: #500160)
  * New Italian debconf translation, thanks to Vince (closes: #504158)

 -- Francois Marier <francois@debian.org>  Mon, 03 Nov 2008 22:21:40 +1300

email-reminder (0.7.3-3) unstable; urgency=low

  * Debconf translations:
    - New Russian translation, thanks to Yuri Kozlov (closes: #476914)
    - New Basque translation, thanks to Piarres Beobide (closes: #479527)
    - New Czech translation, thanks to Miroslav Kure (closes: #480186)
    - New Swedish translation, thanks to Martin Bagge (closes: #483711)

 -- Francois Marier <francois@debian.org>  Sat, 31 May 2008 11:46:29 +1200

email-reminder (0.7.3-2) unstable; urgency=low

  * Update upstream URL in email footers

 -- Francois Marier <francois@debian.org>  Tue, 15 Apr 2008 09:30:24 +1200

email-reminder (0.7.3-1) unstable; urgency=low

  * New upstream release (closes: #475644)

 -- Francois Marier <francois@debian.org>  Sun, 13 Apr 2008 17:53:50 +1200

email-reminder (0.7.2-3) unstable; urgency=low

  * debconf templates:
    - Revised English template, thanks to Justin and Christian
    - New Chinese translation, thanks to Niels He (closes: #473506)
    - New Finnish translation, thanks to Esko Arajärvi (closes: #473443)
    - New Vietnamese translation, thanks to Clytie Siddall (closes: #473391)
    - New Galician translation, thanks to Jacobo Tarrio (closes: #473503)
    - New Georgian translation, thanks to Aiet Kolkhi (closes: #473846)
    - Updated French translation, thanks to Christian Perrier
    - Updated German translation from Helge (closes: #473389)
    - Updated Portugese translation from Américo (closes: #473635)
  * Update the download URL and homepage
  * Bump debhelper compatibility and dependency to 6

 -- Francois Marier <francois@debian.org>  Mon, 07 Apr 2008 00:00:32 +1200

email-reminder (0.7.2-2) unstable; urgency=low

  * Check for the existence of empty perl directory before removing it
    since the bug will be fixed with Perl 5.10 (closes: #468199)

 -- Francois Marier <francois@debian.org>  Thu, 28 Feb 2008 09:29:55 +1300

email-reminder (0.7.2-1) unstable; urgency=high

  * New upstream release
    - Fixes corruption of the .email-reminders file in the presence of accented
      characters (hence the high urgency)
    - Now sends emails as UTF-8, allowing accented characters in event names

  * Move the chmod'ing of the config file before it's written out in order
    to make this safer.  Thanks to Joey Hess for catching this.
  * Add German debconf translation. Thanks Helge! (closes: #462805)
  * Reset the debconf password question after the config has been written.
    This unfortunately means that the question will be ask at every install
    or upgrade, but at least it allows the password to be set to the empty
    string (closes: #462658).
  * debian/control: Move Perl to Build-Depends-Indep
  * debian/rules: Rename empty /usr/lib/perl5 after build
  * debian/postinst: Create the userid as a system user (and remove the group
    from postinst and cron.daily)

 -- Francois Marier <francois@debian.org>  Thu, 21 Feb 2008 18:32:32 +1300

email-reminder (0.7.1-1) unstable; urgency=low

  * New upstream release
  * Update copyright year in debian/copyright
  * Set the right ownership on /etc/email-reminder.conf so that the
    email-reminder user can read it

 -- Francois Marier <francois@debian.org>  Sat, 12 Jan 2008 10:32:00 +1300

email-reminder (0.7.0-2) unstable; urgency=low

  * Add Portugese debconf translation (closes: #458088)
    Thanks to Américo Monteiro!

 -- Francois Marier <francois@debian.org>  Fri, 28 Dec 2007 12:14:40 -0500

email-reminder (0.7.0-1) unstable; urgency=low

  * New upstream release:
    - security enhancements
    - support for SMTP authentication
  * Added postinst and postrm scripts to create a spool directory and a
    restricted user account
  * Updated the cron job to make use of the new collect-reminders script
  * Recommend the packages necessary for SMTP authentication
  * Handle the config file entirely in debconf
  * Use the full download list in the watch file
  * Rename XS-Vcs-* fields to Vcs-*
  * Add a call to dh_desktop in debian/rules
  * Move the build stuff into the indep section
  * Bump Standards-Version to 3.7.3 (no other changes)

 -- Francois Marier <francois@debian.org>  Tue, 11 Dec 2007 23:29:10 +1300

email-reminder (0.6.0-3) unstable; urgency=low

  * Fix watch file (closes: #449844)

 -- Francois Marier <francois@debian.org>  Mon, 19 Nov 2007 17:02:25 +1300

email-reminder (0.6.0-2) unstable; urgency=low

  * debian/copyright: Refer to /usr/share/common-licenses/GPL-3
  * debian/compat: Bump to version 5
  * debian/control:
    - Add homepage field in debian/control
    - Mention collab-maint repository in debian/control
    - Bump debhelper dependency to 5
  * Remove the "Application" category from .desktop file (lintian warning)
  * Fix the whatis entry in the email-reminder-editor manpage (lintian warning)

 -- Francois Marier <francois@debian.org>  Sun, 28 Oct 2007 14:47:44 +1300

email-reminder (0.6.0-1) unstable; urgency=low

  * New upstream release:
    - weekly and monthly recurrences
  * cron.daily: don't output an error message when the script cannot be found (closes: #435990)
  * Include GPLv3 in debian/copyright
  * Switch menu item to Applications/Office (menu transition)
  * Fix "make clean" lintian warning

 -- Francois Marier <francois@debian.org>  Mon, 13 Aug 2007 00:42:22 +1200

email-reminder (0.5.7-1) unstable; urgency=low

  * New upstream version:
    - include the date field in emails which are sent out (closes: #426047)
      Thanks to Ron Guerin for the patch!
  * Bump Standards-Version up to 3.7.2 (no changes)
  * Add a watch file

 -- Francois Marier <francois@debian.org>  Tue, 29 May 2007 16:05:24 +1200

email-reminder (0.5.6-1) unstable; urgency=low

  *  New upstream release:
     - display the full date of the occurrence in advance notifications
       (including events where the year is not specified)
     - support sending events to multiple email addresses
     - fixed typo in anniversary reminder
  * Update FSF address

 -- Francois Marier <francois@debian.org>  Sun, 11 Dec 2005 22:37:52 -0500

email-reminder (0.5.5-1) unstable; urgency=low

  * New upstream release:
    - display the date of the occurence in advance notifications
      (closes: #324217)
  * Set Standards Version to 3.6.2
  * Recommend exim4 as default mta
  * Remove debian/conffiles to remove duplicate config file

 -- Francois Marier <francois@debian.org>  Fri, 26 Aug 2005 22:55:18 -0400

email-reminder (0.5.4-1) unstable; urgency=low

  * New upstream release:
    - fixes the config filename (closes: #312301)

 -- Francois Marier <francois@debian.org>  Tue,  7 Jun 2005 21:56:20 -0400

email-reminder (0.5.3-2) unstable; urgency=low

  * Move mail-transport-agent from "Depends" to "Recommends" since it is
    possible to use an external SMTP server.

 -- Francois Marier <francois@debian.org>  Sat, 14 May 2005 22:25:55 -0400

email-reminder (0.5.3-1) unstable; urgency=low

  * New upstream release:
    - fix for Feb. 29th events (closes: #299779)
    - support for assocating each event with a different email address
    (closes: #299780)

 -- Francois Marier <francois@debian.org>  Mon, 25 Apr 2005 22:49:25 +0200

email-reminder (0.5.2-3) unstable; urgency=low

  * Fixed the dependencies (added cron and an mta)
  * Recommend anacron and comment about it in the description

 -- Francois Marier <francois@debian.org>  Fri, 22 Apr 2005 19:10:13 +0200

email-reminder (0.5.2-2) unstable; urgency=low

  * Updated the download URL in copyright
  * Fixed lintian errors and warnings

 -- Francois Marier <francois@debian.org>  Sat,  5 Feb 2005 14:43:32 -0500

email-reminder (0.5.2-1) unstable; urgency=low

  * New upstream release: minor bug fix
  * Updated copyright file

 -- Francois Marier <francois@debian.org>  Mon, 31 Jan 2005 21:11:27 -0500

email-reminder (0.5.1-1) unstable; urgency=low

  * First version in the Debian archive
  * New upstream release: actually sends out the reminders
  * Fixed URL in copyright file
  * Include original tarball properly so that a diff file is generated

 -- Francois Marier <francois@debian.org>  Wed, 22 Sep 2004 20:12:00 -0400

email-reminder (0.5.0-1) unstable; urgency=low

  * Initial public release
  * Added copyright file

 -- Francois Marier <francois@debian.org>  Tue,  7 Sep 2004 14:21:17 -0400

email-reminder (0.2.0-1) unstable; urgency=low

  * Second private release
  * Added libemail-valid-perl dependency
  * Renamed Debian menu item
  * Install the GNOME menu item

 -- Francois Marier <francois@debian.org>  Wed, 11 Aug 2004 18:18:25 -0400

email-reminder (0.1.0-1) unstable; urgency=low

  * Initial private release

 -- Francois Marier <francois@debian.org>  Wed,  7 Jul 2004 15:53:25 -0400
