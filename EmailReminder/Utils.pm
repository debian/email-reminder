# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::Utils;

# Utility subroutines for the Email-Reminder program

use strict;
use warnings;

use XML::DOM;

our $VERSION = '0.8.3';
our $USER_CONFIG_FILE = '.email-reminders';
our $SPOOL_DIRECTORY = '/var/spool/email-reminder';

my %names = (
    1 => "Paper",
    2 => "Cotton",
    3 => "Leather",
    4 => "Linen",
    5 => "Wood",
    6 => "Iron",
    7 => "Copper",
    8 => "Bronze",
    9 => "Pottery",
    10 => "Tin",
    11 => "Steel",
    12 => "Silk",
    13 => "Lace",
    14 => "Ivory",
    15 => "Crystal",
    20 => "China",
    25 => "Silver",
    30 => "Pearl",
    35 => "Jade",
    40 => "Ruby",
    45 => "Sapphire",
    50 => "Golden",
    55 => "Emerald",
    60 => "Diamond",
);

sub get_node_value
{
    my $node = shift;
    my $tag_name = shift;

    $node = $node->getElementsByTagName($tag_name, 0)->item(0);
    return unless $node;
    
    $node = $node->getFirstChild;
    return unless $node;

    return $node->getNodeValue;
}

sub set_node_value
{
    my ($node, $tag_name, $new_value) = @_;

    my $subnode = $node->getElementsByTagName($tag_name, 0)->item(0);
    if (!defined($subnode))
    {
        $subnode = $node->getOwnerDocument()->createElement($tag_name);
        $node->appendChild($subnode);
        $subnode->addText($new_value);
    }
    else
    {
        my $textnode = $subnode->getFirstChild();
        if (!defined($textnode))
        {
            $subnode->addText($new_value);
        }
        else
        {
            $textnode->setNodeValue($new_value);
        }
    }
    return 1;
}

# Returns the proper English qualifier for this number
sub get_th
{
    my $number = shift;
    return unless defined($number);

    if ($number >= 11 && $number <= 13) {
        return "th";
    } elsif ($number % 10 == 1) {
        return "st";
    } elsif ($number % 10 == 2) {
        return "nd";
    } elsif ($number % 10 == 3) {
        return "rd";
    } else {
        return "th";
    }
}

# Returns the traditional name for this occurence of the anniversary
# if applicable (e.g. Silver, Golden, Diamond)
# (see http://www.the-inspirations-store.com/acatalog/anniversary.html)
sub get_special_name
{
    my $occurence = shift;
    return unless defined($occurence);
    return unless exists $names{$occurence};
    return "($names{$occurence}) ";
}

# Return some general-purpose debugging info
sub debug_info
{
    my $obj = shift;
    my $depth_level = shift;

    my $ret = "\nEmail-reminder: ".$EmailReminder::Utils::VERSION;
    $ret .= "\nPerl: $] ($^O)";

    local $ENV{PATH};
    $ENV{PATH} = ''; # remove potentially tainted path
    $ENV{ENV} = ''; # necessary on SUSE
    my $distro = `/usr/bin/lsb_release -s -d`; chomp $distro;
    my $kernel = `/bin/uname -a`; chomp $kernel;
    $ret .= "\nOS: $distro";
    $ret .= "\nKernel: $kernel";

    use Data::Dumper;
    local $Data::Dumper::Maxdepth;
    $Data::Dumper::Maxdepth = $depth_level;
    $ret .= "\nObject:";
    $ret .= Dumper($obj);
}

1;
