# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::AnniversaryStore;

use strict;
use warnings;

use EmailReminder::AnniversaryEvent;
use EmailReminder::EventStore;

use base qw(EmailReminder::EventStore);

# Column indices
my $PARTNER_NAME_INDEX  = 4;
my $PARTNER_EMAIL_INDEX = 5;

sub init
{
    my ($self) = @_;

    $self->{TYPE} = EmailReminder::AnniversaryEvent->get_type();
    $self->{NB_COLUMNS} = EmailReminder::AnniversaryEvent->get_nb_fields();

    $self->EmailReminder::EventStore::init();
    return 1;
}

sub get_event_column
{
    my ($self, $event, $col) = @_;
    
    if ($col == $PARTNER_NAME_INDEX) {
        return $event->get_partner_name();
    }
    elsif ($col == $PARTNER_EMAIL_INDEX) {
        return $event->get_partner_email();
    }
    else {
        return $self->EmailReminder::BirthdayStore::get_event_column($event, $col);
    }
}

1;
