# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::BirthdayEvent;

use strict;
use warnings;
use overload '""' => \&str;
use EmailReminder::Utils;
use EmailReminder::YearlyEvent;

use base qw(EmailReminder::YearlyEvent);

# XML tags
__PACKAGE__->mk_accessors(qw(email));

sub str {
    my ($self) = @_;
    return $self->get_type . ':' . $self->get_id . ') ' . $self->get_name . ' - ' . $self->get_date;
}

# Hard-coded value for this event's type (class method)
sub get_type
{
    return 'birthday';
}

# Number of fields this event adds to its parent (class method)
sub get_nb_fields
{
    my ($self) = @_;
    return $self->SUPER::get_nb_fields() + 1;
}

sub valid_email
{
    my ($class, $new_value) = @_;
    # ToDo: do checking on the email address
    return $new_value;
}

# Returns the age of the person (starts at 0 years old)
sub get_occurence
{
    my $self = shift;

    my $age = $self->EmailReminder::YearlyEvent::get_occurence();

    return defined($age) ? ($age - 1) : undef;
}

sub get_subject
{
    my $self = shift;

    my $name = $self->get_name();
    my $age  = $self->get_occurence();
    my $when = $self->{"WHEN"} || '';

    if ($age and $when eq "today") {
        return "$name is now $age";
    }
    else {
        return "${name}'s birthday";
    }
}

sub get_message_body
{
    my $self = shift;

    # birthday person
    my $name  = $self->get_name();
    my $email = $self->get_email();
    my $age   = $self->get_occurence();
    my $when  = $self->{"WHEN"} || '';
    
    my $age_string = $age ? "turning $age" : "getting one year older";
    my $email_message = $email ? "\n\nYou can reach $name at $email." : "";

    my $message = <<"MESSAGEEND";
I just want to remind you that $name is $age_string $when.$email_message
MESSAGEEND

    return $message;
}

1;
