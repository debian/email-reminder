# This file is part of Email-Reminder.
#
# Copyright (C) 2020 by Francois Marier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from EmailReminder.Utils import readXMLValue, writeXMLValue, EMAIL_REGEX
from EmailReminder.Birthday import BirthdayModel


class AnniversaryModel(BirthdayModel):
    LABEL = 'Anniversaries'
    TYPE = 'anniversary'
    COLUMN_NAMES = ["Person 1", "Date", "Email 1", "Person 2", "Email 2"]

    def fillNewEvent(self, event):
        super().fillNewEvent(event)
        writeXMLValue(self.document, event, 'partner_name', '')
        writeXMLValue(self.document, event, 'partner_email', '')

    def getDataInternal(self, column, row):
        parentData = super().getDataInternal(column, row)
        if parentData:
            return parentData
        if column == 3:
            return readXMLValue(self.events[row], 'partner_name')
        if column == 4:
            return readXMLValue(self.events[row], 'partner_email')

        return None

    def setDataInternal(self, column, row, value):
        if column == 3:
            writeXMLValue(self.document, self.events[row], 'partner_name',
                          value)
        elif column == 4:
            if not EMAIL_REGEX.fullmatch(value):
                value = ''
            writeXMLValue(self.document, self.events[row], 'partner_email',
                          value)
        else:
            super().setDataInternal(column, row, value)
