# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::AnniversaryEvent;

use strict;
use warnings;
use overload '""' => \&str;
use EmailReminder::Utils;
use EmailReminder::BirthdayEvent;

use base qw(EmailReminder::BirthdayEvent);

# XML tags
__PACKAGE__->mk_accessors(qw(partner_name partner_email));

sub str {
    my ($self) = @_;
    return $self->get_type . ':' . $self->get_id . ') ' . $self->get_name . ' and ' . $self->get_partner_name . ' - ' . $self->get_date;
}

# Hard-coded value for this event's type (class method)
sub get_type
{
    return 'anniversary';
}

# Number of fields this event adds to its parent (class method)
sub get_nb_fields
{
    my ($self) = @_;
    return $self->SUPER::get_nb_fields() + 2;
}

sub valid_partner_name {
    my ($class, $new_value) = @_;
    return $new_value;
}

sub valid_partner_email {
    my ($class, $new_value) = @_;
    return $new_value;
}

sub get_subject
{
    my $self = shift;

    my $name         = $self->get_name();
    my $partner_name = $self->get_partner_name();
    my $occurence    = $self->get_occurence();
    my $th           = EmailReminder::Utils::get_th($occurence);

    if ($occurence) {
        return "${occurence}$th anniversary of $name and $partner_name";
    }
    else {
        return "Anniversary of $name and $partner_name";
    }
}

sub get_message_body
{
    my $self = shift;

    # people involved
    my $name          = $self->get_name();
    my $email         = $self->get_email();
    my $partner_name  = $self->get_partner_name();
    my $partner_email = $self->get_partner_email();
    my $occurence     = $self->get_occurence();
    my $th            = EmailReminder::Utils::get_th($occurence);
    my $special_name  = EmailReminder::Utils::get_special_name($occurence) || '';
    my $when          = $self->{"WHEN"} || '';

    my $subject = $occurence ? "${occurence}$th anniversary of $name and $partner_name" : "Anniversary of $name and $partner_name";
    my $occurence_string = $occurence ? "${occurence}$th " : "";

    my $email_message = "";
    if ($email && $partner_email) {
        $email_message = "\n\nYou can reach them at $email and $partner_email respectively.";
    } elsif ($email) {
        $email_message = "\n\nYou can reach $name at $email.";
    } elsif ($partner_email) {
        $email_message = "\n\nYou can reach $partner_name at $partner_email.";
    }

    my $message = <<"MESSAGEEND";
I just want to remind you that the ${occurence_string}anniversary ${special_name}of $name and $partner_name is $when.$email_message
MESSAGEEND

    return $message;
}

1;
