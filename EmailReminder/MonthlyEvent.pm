# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::MonthlyEvent;

use strict;
use warnings;
use overload '""' => \&str;
use Date::Manip;
use POSIX;
use Scalar::Util;

use EmailReminder::Event;
use EmailReminder::Utils;

use base qw(EmailReminder::Event);

# XML tags
__PACKAGE__->mk_accessors(qw(day));

# Global date variables
my $current_time = ParseDate("now");
my $current_date = ParseDate(UnixDate($current_time, "\%x"));
my $current_month = UnixDate($current_time, "\%m");
my $current_year = UnixDate($current_time, "\%Y");

sub str {
    my ($self) = @_;
    return $self->get_type . ':' . $self->get_id . ') ' . $self->get_name . ' - ' . $self->get_day;
}

# Hard-coded value for this event's type (class method)
sub get_type
{
    return 'monthly';
}

# Number of fields this event adds to its parent (class method)
sub get_nb_fields
{
    my ($self) = @_;
    return $self->SUPER::get_nb_fields() + 2;
}

sub valid_day
{
    my ($class, $new_value) = @_;

    if (!Scalar::Util::looks_like_number($new_value)) {
        $new_value = 1;
    }

    # Make sure the value is a valid number
    if ($new_value > 31) {
        $new_value = 31;
    } elsif ($new_value < 1) {
        $new_value = 1;
    }
    
    return $new_value;
}

sub get_current_occurence_date
{
    my ($self, $modifier) = @_;

    my $day = $self->get_day();
    return unless $day;

    # Set the day of the month where the event occurs, make sure the date is valid
    # (e.g. fix-up for event on the 31st when the month has only 30 days)
    my $current_occurence_date = ParseDate("$current_year-$current_month-$day");
    while ($current_occurence_date eq "" or UnixDate($current_occurence_date, "\%d") != $day) {
        $day -= 1;
        $current_occurence_date = ParseDate("$current_year-$current_month-$day");
    }

    my $modified_date = $current_occurence_date;
    if ($modifier) {
        if ($modifier >= $day) {
            # We are warning about an event in a few days, past the end of the current month
            $modified_date = DateCalc($modified_date, " + 1 month");
        }
        $modified_date = DateCalc($modified_date, " - $modifier days");
    }

    return $modified_date;
}

sub get_subject
{
    my $self = shift;
    return $self->get_name();
}

sub get_message_body
{
    my $self = shift;

    # event details
    my $when      = $self->{"WHEN"} || '';
    my $name      = $self->get_name();
    
    my $message = <<"MESSAGEEND";
I just want to remind you of the following event $when:

$name
MESSAGEEND

    return $message;
}

# Returns 1 if the event will occur in X days (X is a param)
sub will_occur
{
    my $self = shift;
    my $modifier = shift;
    
    # Apply the modifier to the event date
    my $current_occurence_date = $self->get_current_occurence_date($modifier);
    return 0 unless $current_occurence_date;
    
    my $tomorrow = DateCalc($current_date, " + 1 day");
    if (Date_Cmp($current_date, $current_occurence_date) == 0) {
        return 1;
    } elsif (Date_Cmp($current_date, $current_occurence_date) < 0 and
        Date_Cmp($tomorrow, $current_occurence_date) > 0) {
        # e.g. if today is the 30, the reminder is on the 31st and tomorrow is the 1st
        return 1;
    } else {
        return 0;
    }
}

1;
