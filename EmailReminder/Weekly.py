# This file is part of Email-Reminder.
#
# Copyright (C) 2020 by Francois Marier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from EmailReminder.Event import EventModel, parseDay
from EmailReminder.Utils import readXMLValue, writeXMLValue


class WeeklyModel(EventModel):
    LABEL = 'Weekly Events'
    TYPE = 'weekly'
    COLUMN_NAMES = ["Event Name", "Day of the Week"]

    def fillNewEvent(self, event):
        super().fillNewEvent(event)
        writeXMLValue(self.document, event, 'day', 1)

    def getDataInternal(self, column, row):
        parentData = super().getDataInternal(column, row)
        if parentData:
            return parentData
        if column == 1:
            return readXMLValue(self.events[row], 'day')

        return None

    def setDataInternal(self, column, row, value):
        if column == 1:
            writeXMLValue(self.document, self.events[row], 'day',
                          parseDay(value, 7))
        else:
            super().setDataInternal(column, row, value)
