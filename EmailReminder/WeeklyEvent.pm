# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::WeeklyEvent;

use strict;
use warnings;
use overload '""' => \&str;
use Date::Manip;
use POSIX;
use Scalar::Util;

use EmailReminder::Event;
use EmailReminder::Utils;

use base qw(EmailReminder::Event);

# XML tags
__PACKAGE__->mk_accessors(qw(day));

# Global date variables
my $current_time = ParseDate("now");
my $current_date = ParseDate(UnixDate($current_time, "\%x"));
my $current_dayofweek = UnixDate($current_time, "\%w");

sub str {
    my ($self) = @_;
    return $self->get_type . ':' . $self->get_id . ') ' . $self->get_name . ' - ' . $self->get_day;
}

# Hard-coded value for this event's type (class method)
sub get_type
{
    return 'weekly';
}

# Number of fields this event adds to its parent (class method)
sub get_nb_fields
{
    my ($self) = @_;
    return $self->SUPER::get_nb_fields() + 2;
}

sub valid_day
{
    my ($class, $new_value) = @_;

    if (!Scalar::Util::looks_like_number($new_value)) {
        # Try to parse as a string
        my $day = UnixDate(ParseDate($new_value), "\%w");
        if ($day) {
            $new_value = $day;
        } else {
            $new_value = 7; # Default: Sunday
        }
    }

    if ($new_value > 7 or $new_value < 1) {
        # Default to Sunday for out of range dates (since zero is
        # both 0 and 7).
        $new_value = 7;
    }

    return $new_value;
}

sub get_subject
{
    my $self = shift;
    return $self->get_name();
}

sub get_message_body
{
    my $self = shift;

    # event details
    my $when      = $self->{"WHEN"} || '';
    my $name      = $self->get_name();
    
    my $message = <<"MESSAGEEND";
I just want to remind you of the following event $when:

$name
MESSAGEEND

    return $message;
}

# Returns 1 if the event will occur in X days (X is a param)
sub will_occur
{
    my $self = shift;
    my $modifier = shift;
    
    # Apply the modifier to the event date
    my $modified_day = $self->get_day();
    return 0 unless $modified_day;

    if ($modifier) {
        $modified_day -= $modifier;
        while ($modified_day > 7) {
            $modified_day -= 7;
        }
        while ($modified_day < 1) {
            $modified_day += 7;
        }
    }
    
    if ($current_dayofweek == $modified_day) {
        return 1;
    } else {
        return 0;
    }
}

1;
