# This file is part of Email-Reminder.
#
# Email-Reminder is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Email-Reminder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Email-Reminder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

package EmailReminder::EventStore;

# Base class for all of the event stores.
# 
# This class should never be used directly, use a derived class instead.

use strict;
use warnings;

sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;
    return $self;
}

sub init
{
    my ($self) = @_;
    $self->{EVENTS} = [];
    $self->{NB_EVENTS} = 0;
    return 1;
}

sub add_event
{
    my ($self, $event) = @_;
    push (@{$self->{EVENTS}}, $event);
    $self->{NB_EVENTS}++;
}

sub get_nb_events
{
    my ($self) = @_;
    return $self->{NB_EVENTS};
}

sub get_events
{
    my ($self) = @_;
    return $self->{EVENTS};
}

1;
