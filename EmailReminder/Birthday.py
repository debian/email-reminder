# This file is part of Email-Reminder.
#
# Copyright (C) 2020 by Francois Marier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from EmailReminder.Utils import readXMLValue, writeXMLValue, EMAIL_REGEX
from EmailReminder.Yearly import YearlyModel


class BirthdayModel(YearlyModel):
    LABEL = 'Birthdays'
    TYPE = 'birthday'
    COLUMN_NAMES = ["Name", "Birth Date", "Email"]

    def fillNewEvent(self, event):
        super().fillNewEvent(event)
        writeXMLValue(self.document, event, 'email', '')

    def getDataInternal(self, column, row):
        parentData = super().getDataInternal(column, row)
        if parentData:
            return parentData
        if column == 2:
            return readXMLValue(self.events[row], 'email')

        return None

    def setDataInternal(self, column, row, value):
        if column == 2:
            if not EMAIL_REGEX.fullmatch(value):
                value = ''
            writeXMLValue(self.document, self.events[row], 'email', value)
        else:
            super().setDataInternal(column, row, value)
